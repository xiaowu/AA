from flask import Flask,json,Blueprint,render_template,request,redirect,url_for,session,g,jsonify
from models import User, Car , Order
import config
from exts import db
import random
from sqlalchemy import or_
from datetime import datetime
import os
from flask_cors import CORS
from decorators import login_required