#!/usr/bin/python
# -*- coding: UTF-8 -*-

from flask import Flask,json,render_template,request,redirect,url_for,session,g,jsonify
from models import User, Car , Order
import config
from exts import db
import random
from sqlalchemy import or_
from datetime import datetime
import os
from flask_cors import CORS
from decorators import login_required

# from flask_login import LoginManager,login_required,UserMixin,login_user,logout_user

app=Flask(__name__)
app.config.from_object(config)
db.init_app(app)
CORS(app, supports_credentials=True)




@app.route('/api/V1.0/index/',methods=['GET','POST'])
def index():
	if method == 'GET':
		return	render_template('index.html')
	else:
		res=request.get_data()
		s=json.loads(res)
		for v in s.values():
			orig = v['orig']
			dest = v['dest']
			date = v['date']
		if dest == None:
			return jsonify({'error':'请输入终点城市！'})	
		else:
			cars = [dict(id = x.id, ticket = x.ticket, price = x.price, orig = x.orig, dest = x.dest, date = x.date, time = x.time)
				for x in Car.query.order_by(Car.orig == orig, Car.dest == dest, Car.date == date).all()]
			if cars :
				return jsonify({'car':cars})
		 	else:
				return jsonify({'error':'线路尚未开通或该日期无班次！'})


# 客户端获取具体班次信息 OK
@app.route('/api/V1.0/details/<int:car_id>',methods=['GET'])
# @login_required
def details_car(car_id):
	x = Car.query.filter(Car.id == car_id).first()
	car = [dict(id = x.id, ticket = x.ticket, price = x.price, orig = x.orig, dest = x.dest, date = x.date, time = x.time)]
	if car :
		return jsonify({'car': car})
	else:
		return jsonify({'error':'访数据库出错啦！'})




# 订票  OK-------------------------------------------------
@app.route('/api/V1.0/details/<int:car_id>',methods = ['POST'])
# @login_required
def book_car(car_id):
	x = Car.query.filter(Car.id == car_id).first()
	car = [dict(id = x.id, ticket = x.ticket, price = x.price, orig = x.orig, dest = x.dest, date = x.date, time = x.time)]

	res = request.get_data()
	s = json.loads(res)
	for v in s.values():
		ticket_num=v['ticket_num']
		
	carid = car_id
	time = datetime.now()
	now = time.strftime('%Y%m%d%H%M')
	sj = str(random.randint(100,999))
	order_num = now+sj
	state = '未乘坐'
	# auth_id = session['user_id']
	auth_id = 1
	order = Order(ticket_num = ticket_num, carid = carid, order_num = order_num , state = state, auth_id =auth_id , create_time = datetime.now())
	ticket = car[0]['ticket']
	surplus =  str(int(ticket)-int(ticket_num))
	print surplus
	if surplus<0 :
		return jsonify ({'error':'余票不足！'})
	else:
		x.ticket = surplus
		db.session.add(order)
		db.session.commit()
		return jsonify({'result':'True'})

#客户端查询订单   OK
@app.route('/api/V1.0/order/',methods = ['GET'])
@login_required
def get_order():
	id = session['user_id']
	# id = 1
	auth = User.query.filter(User.id == id).first()
	s = auth.orders
	order = [dict(id = x.id, ticket_num = x.ticket_num, create_time = x.create_time, order_num = x.order_num, state = x.state, carid = x.carid, auth_id = x.auth_id) for x in s]
	if s == None:
		return jsonify({'result':'你目前还没有订单！'})
	else:
		return jsonify({'orders': order})


# 客户端获取订单内具体车辆信息 OK
@app.route('/api/V1.0/order/<int:order_id>',methods = ['GET'])
# @login_required
def get_car_msg(order_id):
	order = Order.query.filter(Order.id == order_id).first()
	car_id = order.carid
	x = Car.query.filter(Car.id ==car_id).first()
	car = [dict(id = x.id, ticket = x.ticket, price = x.price, orig = x.orig, dest = x.dest, date = x.date, time = x.time)]
	return jsonify({'car': car})


# 客户端修改订单信息 OK
@app.route('/api/V1.0/order/<int:order_id>',methods = ['PUT'])
# @login_required
def revise_order(order_id):
	order = Order.query.filter(Order.id == order_id).first()
	car = Car.query.filter(Car.id == order.carid).first()
	res = request.get_data()
	s = json.loads(res)
	for v in s.values():
		ticket_num = v['ticket_num']
	s = order.ticket_num
	cha = str(int(s)-int(ticket_num))	
	order.ticket_num = ticket_num
	ticket = car.ticket
	car.ticket = str(int(ticket)+int(cha))
	if car.ticket<0 :
		return jsonify ({'error':'余票不足！'})
	else:
		db.session.commit()
		return jsonify({'result':'True'})


# 客户端删除个人订单 OK
@app.route('/api/V1.0/order/<int:order_id>',methods = ['DELETE'])
# @login_required
def delete_order(order_id):
	# id = session['user_id']
	id=1
	order = Order.query.filter(Order.id == order_id).first()
	car = Car.query.filter(Car.id == order.carid) .first()
	if order :
		ticket = car.ticket
		ticket_num = order.ticket_num
		car.ticket = str(int(ticket) + int(ticket_num))
		db.session.delete(order)
		db.session.commit()
		return jsonify({'result':'True'})
	else:
		return jsonify({'error':'你目前还没有订单！'})


#------------------manager------------------




# 后台发布车辆信息 OK
@app.route('/api/manager/V1.0/car/',methods = ['POST'])
# @login_required
def pub_cars():
#revise user_id to manager_id
	# manager = session['user_id']
	# if manager:
		res = request.get_data()
		s=json.loads(res)
		for v in s.values():
			ticket = v['ticket']
			price = v['price']
			date = v['date']
			orig = v['orig']
			dest = v['dest']
			time = v['time']
		s = Car.query.filter(Car.date == date , Car.orig == orig , Car.dest == dest , Car.time == time).first()
		if s :
			return jsonify({'error':'该班次已存在！'})
		else:
			car = Car(ticket = ticket, price = price, date = date, orig = orig, dest = dest, time = time)	
			if car:
				db.session.add(car)
				db.session.commit()
				return jsonify({'result':'True'})
			else:
				return jsonify({'error':'请输入必填项!'})
	# else:
	# 	return jsonify({'error':'管理员才有这权限哦！'})

#manager修改车辆信息  OK
@app.route('/api/manager/V1.0/car/<int:car_id>',methods = ['PUT'])
# @login_required
def revise_car(car_id):
#revise user_id to manager_id
	# manager = session['user_id']
		car = Car.query.filter(Car.id == car_id).first()
	# if manager:
		res = request.get_data()
		s=json.loads(res)
		for v in s.values():
			ticket = v['ticket']
			price = v['price']
			date = v['date']
			orig = v['orig']
			dest = v['dest']
			time = v['time']

		car.ticket = ticket
		car.price = price
		car.date = date
		car.orig = orig
		car.dest = dest
		car.time = time
		db.session.commit()
		return jsonify({'result':'True'})
	# else :
	# 	return jsonify({'error':'管理员才有这权限哦！'})


#manager 删除车辆信息 OK
@app.route('/api/manager/V1.0/car/<int:car_id>',methods = ['DELETE'])
# @login_required
def delete_car(car_id):
	# manager = session['user_id']
		car = Car.query.filter(Car.id == car_id).first()
	# if manager:
		if car:
			db.session.delete(car)
			db.session.commit()
			return jsonify({'result':'True'})
		else:
			return jsonify({'error':'访问数据库出错啦！'})
	# else:
	# 	return jsonify({'error':'管理员才有这权限哦！'})



# manager 查询发布的全部车辆信息  OK
@app.route('/api/manager/V1.0/car/',methods = ['GET'])
# @login_required
def get_car():
	# manager = session['user_id']
	# if manager:

		cars = [dict(id = x.id, ticket = x.ticket, price = x.price, orig = x.orig, dest = x.dest, date = x.date, time = x.time)
			for x in Car.query.order_by('-id').all()]
		if cars == None:
			return jsonify({'error':'还没有发布车辆!'})
		else:

			return jsonify({'car': cars})
	# else:
	# 	return jsonify({'error':'管理员才有这权限哦！'})


#manager 获取订单情况   OK
@app.route('/api/manager/V1.0/order/',methods = ['GET'])
# @login_required
def get_orders():

	# manager = session['user_id']
	# if manager:
		s = Order.query.order_by('-id').all()

		orders = [dict(id = x.id, ticket_num = x.ticket_num, create_time = x.create_time, 
			order_num = x.order_num, state = x.state, carid = x.carid, auth_id = x.auth_id) for x in s]
		if s:
			return jsonify({'orders': orders})
		else:
			return jsonify({'error':'目前还没有订单！'})
	# else:
	# 	return jsonify({'error':'管理员才有这权限哦！'})


# manager 确认乘客已经乘车  OK
@app.route('/api/manager/V1.0/order/<int:order_id>',methods = ['PUT'])
# @login_required
def revise_orders(order_id):
	# manager = session['user_id']
	# if manager:
		order = Order.query.filter(Order.id == order_id).first()
		res = request.get_data()
		s = json.loads(res)
		for v in s.values():
			state = v['state']
		order.state = state
		db.session.commit()
		return jsonify({'result':'True'}) 
	# else:
	# 	return jsonify({'error':'管理员才有这权限哦！'})









@app.route('/api/V1.0/index',methods=['GET'])
@login_required
def index():
	return render_template('index.html')

# # OK
# @app.route('/api/V1.0/login',methods=['POST'])
# def login():
# 	res = request.get_data()
# 	s = json.loads(res)
# 	for v in s.values():
# 		telephone = v['telephone']
# 		password = v['password']
# 	user = User.query.filter(User.telephone==telephone).first()
# 	if user and user.check_password(password) :
# 		session['user_id']=user.id
# 		return	jsonify({'result':'Ture'})
# 		# return render_template('login.html')
# 	else:
# 			return jsonify({'error':'手机号码或密码错误！'})



@app.route('/api/V1.0/regist',methods=['POST'])
def regrst():
	res = request.get_data()
	s = json.loads(res)
	for v in s.values():
		telephone = v['telephone']
		username = v['username']
		password = v['password']
	user = User.query.filter(User.telephone==telephone).first()
	if user :
		return jsonify({'error':'手机号已经注册过了!'})
	else :
		user = User(telephone = telephone,username = username, password = password)
		db.session.add(user)
		db.session.commit()
	return jsonify({'result':'True'})



@app.before_request
def my_before_request():
	user_id=session.get('user_id')
	if user_id:
		user=User.query.filter(User.id==user_id).first()
		if user:
			g.user=user

if __name__ == '__main__':
	app.run()






