#encoding: utf-8

from exts import db
from datetime import datetime
from werkzeug.security import generate_password_hash,check_password_hash

class User(db.Model):
	__tablename__='user'
	id=db.Column(db.Integer,primary_key=True,autoincrement=True)
	telephone=db.Column(db.String(11),nullable=False)
	username=db.Column(db.String(50),nullable=False)
	password=db.Column(db.String(100),nullable=False)

#密码通过hash加密
	def __init__(self,*args,**kwargs):
		telephone=kwargs.get('telephone')
		username=kwargs.get('username')
		password=kwargs.get('password')

		self.telephone=telephone
		self.username=username
		self.password=generate_password_hash(password)
#检查密码
	def check_password(self,raw_password):
		result=check_password_hash(self.password,raw_password)
		return result	


class Car(db.Model):
	__tablename__='car'
	id=db.Column(db.Integer,primary_key=True,autoincrement=True)
	ticket=db.Column(db.String(100),nullable=False)
	price=db.Column(db.String(100),nullable=False)
	date=db.Column(db.String(100),nullable=False)
	orig=db.Column(db.String(100),nullable=False)
	dest=db.Column(db.String(100),nullable=False)
	time=db.Column(db.String(100),nullable=False)
	

class Order(db.Model):
	__tablename__='orders'
	id=db.Column(db.Integer,primary_key=True,autoincrement=True)
	ticket_num=db.Column(db.String(100),nullable=False)
	create_time=db.Column(db.DateTime,default=datetime.now)
	order_num=db.Column(db.String(50),nullable=False)
	state=db.Column(db.String(100),nullable=False)
	carid=db.Column(db.Integer,nullable=False)
	auth_id=db.Column(db.Integer,db.ForeignKey('user.id'))
	auth=db.relationship('User',backref=db.backref('orders',order_by=id.desc()))



